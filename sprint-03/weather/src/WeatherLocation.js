import React, { useEffect, useState } from "react";
import { useGeolocation } from "react-use";
import { getWeather } from "./util";

const delay = (ms = 2000) => new Promise((resolve) => setTimeout(resolve, ms));

export const WeatherLocation = () => {
  const [loading, setLoading] = useState(false);
  const [result, setResult] = useState(null);
  const locationData = useGeolocation();

  useEffect(() => {
    const loadData = async () => {
      try {
        setLoading(true);
        setResult(await getWeather(locationData));
      } catch {
        console.log("unable to get weather");
      } finally {
        setLoading(false);
      }
    };
    loadData();
  }, [locationData]);

  if (loading || locationData.loading) {
    return (
      <>
        <button onClick={() => cancel()}>Cancel!</button>
        <p>Loading...</p>
      </>
    );
  }

  if (!result) {
    return (
      <>
        <button onClick={() => setCount((num) => num + 1)}>Make it lag!!</button>
      </>
    );
  }

  return (
    <>
      <p>{JSON.stringify(result)}</p>
      <button onClick={() => setCount((num) => num + 1)}>Make it lag!!</button>
    </>
  );
};

export default WeatherLocation;
