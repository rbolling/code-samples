import React from "react";
import useCounter from "./useCounter";

const Counter = () => {
  const counter = useCounter();

  if (counter.count > 5) {
    return <h1>Count is too high!</h1>;
  }

  return (
    <>
      <h1>{counter.count}</h1>
      <button onClick={counter.increaseCounter}>+</button>
      <button onClick={counter.decreaseCounter}>-</button>
    </>
  );
};

export default Counter;
