import React from "react";
import { Switch, Route, useRouteMatch, Link } from "react-router-dom";
import { LocationByIdScreen } from "./LocationByIdScreen";

export const FilterScreen = ({ locations, filter }) => {
  const match = useRouteMatch();
  return (
    <>
      <h1>Showing Filtered Locations</h1>
      <Switch>
        <Route exact path={`${match.path}`}>
          <ol>
            <li>
              <Link to={`${match.path}/this-does-not-exist`}>
                This def doesnt work
              </Link>
            </li>
            {locations.filter(filter).map((loc) => (
              <li key={loc.id}>
                <Link to={`${match.path}/${loc.id}`}>{loc.place}</Link>
              </li>
            ))}
          </ol>
        </Route>
        <Route path={`${match.path}/:filterId`}>
          <LocationByIdScreen locations={locations} />
        </Route>
      </Switch>
    </>
  );
};
