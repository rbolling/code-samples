import React from "react";
// import "./App.css";
import { Grid } from "@fluentui/react-northstar";
import { v4 as uuidv4 } from "uuid";
import { Screens } from "./screens";
import { Header } from "./components/Header";
import { Footer } from "./components/Footer";
import { Content } from "./components/Content";
import { Sidebar } from "./components/Sidebar";

class App extends React.Component {
  state = {
    locations: [{ id: uuidv4(), place: 75650 }],
  };

  // This adds a new location to this.state.locations
  submitForm = (place) => {
    if (!place) {
      alert("Input field should not be empty");
      return;
    }
    this.setState((state) => {
      const newPlace = { id: uuidv4(), place };
      const newLocation = [...state.locations, newPlace];
      return { locations: newLocation };
    });
  };

  // This removes a location from this.state.locations
  removePlace = (ev) => {
    const removeMe = ev.target.getAttribute("data-placeid");
    this.setState((state) => {
      const locations = state.locations.filter((loc) => loc.id !== removeMe);
      return { locations };
    });
  };

  // This is a custom filter for showing only locations
  // that have integeres as the place
  showOnlyNumbers = (location) => {
    return !Number.isNaN(Number.parseInt(location.place));
  };

  render() {
    const { locations } = this.state;
    return (
      <Grid columns="repeat(2, 1fr)">
        <Header />
        <Content
          leftComponent={
            <Sidebar
              locations={locations}
              submitForm={this.submitForm}
              removePlace={this.removePlace}
            />
          }
        >
          <Screens locations={locations} filter={this.showOnlyNumbers} />
        </Content>
        <Footer />
      </Grid>
    );
  }
}

export default App;
