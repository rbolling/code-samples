//  This is rendering something
const Time = (props) => {
  if (!props.date) {
    return "No time supplied :(";
  }

  if (props.format === "fancy") {
    return makeDateFancy(props.date);
  }
  return props.date.toString();
};

function makeDateFancy(date) {
  return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
}

// Not using state so no need to make "Time" a class
// class TimeClass extends React.Component {
//   render() {
//     if (!this.props.date) {
//       return "No time supplied :(";
//     }
//     return this.props.date.toString();
//   }
// }
