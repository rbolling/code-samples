import React, {useState} from "react";
import "./index.css";
import todosList from "./todos.json";

function App() {
  const [todos, setTodos] = useState(todosList);
  const [inputValue, setInputValue] = useState("");

  const handleDelete = id => event => {
    const newTodos = todos.filter(todo => todo.id !== id);
    setTodos(newTodos);
  };

  const handleToggleComplete = id => event => {
    const newTodos = todos.map(todo => {
      if (todo.id === id) {
        return {
          ...todo,
          completed: !todo.completed
        };
      }

      return todo;
    });
    setTodos(newTodos);
  };

  const handleDeleteCompleted = event => {
    const newTodos = todos.filter(todo => todo.completed !== true);
    setTodos(newTodos);
  };

  const handleCreate = event => {
    if (event.key === "Enter") {
      const newTodos = todos.slice();
      newTodos.push({
        userId: 1,
        id: Math.ceil(Math.random() * 1000000),
        title: inputValue,
        completed: false
      });
      setTodos(newTodos);
      setInputValue("");
    }
  };

  const handleChange = event => {
    setInputValue(event.target.value);
  };

  return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
              className="new-todo"
              placeholder="What needs to be done?"
              autoFocus
              onChange={handleChange}
              value={inputValue}
              onKeyDown={handleCreate}
          />
        </header>
        <TodoList
            todos={todos}
            handleToggleComplete={handleToggleComplete}
            handleDelete={handleDelete}
        />
        <footer className="footer">
        <span className="todo-count">
          <strong>0</strong> item(s) left
        </span>
          <button
              className="clear-completed"
              onClick={handleDeleteCompleted}
          >
            Clear completed
          </button>
        </footer>
      </section>
  );
}

function TodoItem(props) {
  return (<li className={props.completed ? "completed" : ""}>
    <div className="view">
      <input
          className="toggle"
          type="checkbox"
          defaultChecked={props.completed}
          onClick={props.handleToggleComplete}
      />
      <label>{props.title}</label>
      <button className="destroy" onClick={props.handleDelete}/>
    </div>
  </li>);
}

function TodoList(props) {
  return (<section className="main">
    <ul className="todo-list">
      {props.todos.map(todo => (<TodoItem
          key={todo.id}
          title={todo.title}
          completed={todo.completed}
          handleToggleComplete={props.handleToggleComplete(todo.id)}
          handleDelete={props.handleDelete(todo.id)}
      />))}
    </ul>
  </section>);
}

export default App;
